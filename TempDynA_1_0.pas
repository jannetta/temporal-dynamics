unit TempDynA_1_0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    Button2: TButton;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    Edit4: TEdit;
    Button3: TButton;
    Label5: TLabel;
    Edit5: TEdit;
    Button4: TButton;
    Label6: TLabel;
    Edit6: TEdit;
    Button5: TButton;
    Label7: TLabel;
    Edit7: TEdit;
    Label8: TLabel;
    Edit8: TEdit;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Label9: TLabel;
    Edit9: TEdit;
    Button12: TButton;
    Button13: TButton;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Edit10: TEdit;
    Label13: TLabel;
    Edit11: TEdit;
    Button14: TButton;
    Label14: TLabel;
    SaveDialog1: TSaveDialog;
    procedure Button9Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  NDbmp:array[1..4] of TBitmap;
  v,vsmooth,vdtrend,vdtsmooth,vdisplay,lcslope,fwslope,bwslope:array[1..33000,1..4] of double;
  tdmxsl,tdmnsl,tdtpbg,tdtpen,tdbtbg,tdbten:array[1..4,1..4,1..1000] of integer;
  drmxsl,drmnsl,drtpbg,drtpen,drbtbg,drbten:array[1..4,1..4,1..10] of integer;
  imax:array[1..4,1..4,1..6] of integer;
  mean,stdv:array[1..4,1..4,1..6] of double;  
  dscount,tmax,tstart,tend:integer;
  isCalcPoints:boolean;
  maxslope,minslope,topbg,topend,botbg,botend:array[1..33000,1..4] of boolean;
  {red, green, yellow, purple, blue, olive}

implementation

{$R *.DFM}

procedure DrawData;
var t,k,i,j,u,v:integer;
    mx,mn:array[1..4] of double;

begin
     for k:=1 to dscount do
         begin
         mx[k]:=-10000;
         mn[k]:=10000;
         end;
     for k:=1 to dscount do
         for t:=tstart to tend do
             begin
             if vdisplay[t,k]<mn[k] then
                mn[k]:=vdisplay[t,k];
             if vdisplay[t,k]>mx[k] then
                mx[k]:=vdisplay[t,k];
             end;
     for k:=1 to dscount do
         for j:=1 to 200 do
             for i:=1 to 400 do
                 NDbmp[k].Canvas.Pixels[i,j]:=clWhite;
     for k:=1 to dscount do
         for t:=tstart to tend do
             begin
             i:=trunc(400*(t-tstart)/(tend-tstart))+1;
             j:=trunc(200*(vdisplay[t,k]-mn[k])/(mx[k]-mn[k]))+1;
             NDbmp[k].Canvas.Pixels[i,j]:=clBlack;
             end;

     if isCalcPoints then
        begin
        for k:=1 to dscount do
            for t:=tstart to tend do
                begin
                i:=trunc(400*(t-tstart)/(tend-tstart))+1;
                j:=trunc(200*(vdisplay[t,k]-mn[k])/(mx[k]-mn[k]))+1;

                if maxslope[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clRed;

                if minslope[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clGreen;

                if topbg[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clYellow;

                if topend[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clPurple;

                if botbg[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clBlue;

                if botend[t,k] then
                   for u:=-2 to 2 do
                       for v:=-2 to 2 do
                           NDbmp[k].Canvas.Pixels[i+u,j+v]:=clOlive;

                end;
        end;

end;

procedure TForm1.Button9Click(Sender: TObject);
var f:TextFile;s,s1,s2,s3,fn:string;
    i,j,t,u,bm:integer;
    df:double;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;
     isCalcPoints:=false;
     OpenDialog1.Execute;
     fn:=OpenDialog1.FileName;
     AssignFile(f,fn);
     Reset(f);
     readln(f,s);
     readln(f,s);
     t:=0;
     while not(eof(f)) do
           begin
           readln(f,s);
           t:=t+1;
           i:=1;
           s1:='';
           while (i<=Length(s)) and (s[i]<>chr(9)) do
                 begin
                 s1:=s1+s[i];
                 i:=i+1;
                 end;
           i:=i+1;
           s2:='';
           while (i<=Length(s)) and (s[i]<>chr(9)) do
                 begin
                 s2:=s2+s[i];
                 i:=i+1;
                 end;
           i:=i+1;
           s3:='';
           while (i<=Length(s)) and (s[i]<>chr(9)) do
                 begin
                 s3:=s3+s[i];
                 i:=i+1;
                 end;
           v[t,1]:=0;
           v[t,2]:=0;
           v[t,3]:=0;
           if length(s1)>0 then
              v[t,1]:=strtofloat(s1);
           if length(s2)>0 then
              v[t,2]:=strtofloat(s2);
           if length(s3)>0 then
              v[t,3]:=strtofloat(s3);
           end;
     CloseFile(f);
     tmax:=t;
     for t:=1 to tmax do
         for i:=1 to 3 do
             begin
             vsmooth[t,i]:=v[t,i];
             vdtrend[t,i]:=v[t,i];
             vdtsmooth[t,i]:=v[t,i];
             vdisplay[t,i]:=v[t,i];
             end;
     tstart:=1;
     tend:=tmax;
     dscount:=3;
     dscount:=2;
     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];
     Edit2.Text:=inttostr(tmax);
     Label11.Caption:=inttostr(tmax);
     Refresh;
     Label14.Caption:='Ready ';
     Label14.Refresh;
end;

procedure TForm1.Button2Click(Sender: TObject);
var k,t,i,j,sm,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     sm:=strtoint(Edit3.Text);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=trunc(t-sm/2);
             te:=trunc(t+sm/2);
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             vsmooth[t,k]:=0;
             for i:=ts to te do
                 vsmooth[t,k]:=vsmooth[t,k]+v[i,k];
             vsmooth[t,k]:=vsmooth[t,k]/(te-ts+1);
             end;
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             vdtrend[t,k]:=vsmooth[t,k];
             vdtsmooth[t,k]:=vsmooth[t,k];
             vdisplay[t,k]:=vsmooth[t,k];
             end;

     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];

     Label14.Caption:='Ready   ';
     Refresh;

end;

procedure TForm1.Button3Click(Sender: TObject);
var k,t,i,j,dt,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     dt:=strtoint(Edit4.Text);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=t;
             te:=t+dt;
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             vdtrend[t,k]:=0;
             for i:=ts to te do
                 vdtrend[t,k]:=vdtrend[t,k]+vsmooth[i,k];
             vdtrend[t,k]:=vdtrend[t,k]/(te-ts+1);
             vdtrend[t,k]:=vsmooth[t,k]-vdtrend[t,k];
             end;
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             vdtsmooth[t,k]:=vdtrend[t,k];
             vdisplay[t,k]:=vdtrend[t,k];
             end;
     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];

     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button4Click(Sender: TObject);
var k,t,i,j,pt,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     pt:=strtoint(Edit5.Text);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=trunc(t-pt/2);
             te:=trunc(t+pt/2);
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             vdtsmooth[t,k]:=0;
             for i:=ts to te do
                 vdtsmooth[t,k]:=vdtsmooth[t,k]+vdtrend[i,k];
             vdtsmooth[t,k]:=vdtsmooth[t,k]/(te-ts+1);
             end;
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             vdisplay[t,k]:=vdtsmooth[t,k];
             end;
     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];

     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button5Click(Sender: TObject);
var k,t,i,j,sl,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     sl:=strtoint(Edit6.Text);
     sl:=trunc(sl/2);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=t-sl;
             te:=t+sl;
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             lcslope[t,k]:=0;
             for i:=ts to te do
                 lcslope[t,k]:=lcslope[t,k]+(i-t)*vdtsmooth[i,k];
             lcslope[t,k]:=-3*lcslope[t,k]/(sl*(sl+1)*(2*sl+1));
             end;
     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button6Click(Sender: TObject);
var k,t,i,j,fws,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     fws:=strtoint(Edit7.Text);
     fws:=trunc(fws/2);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=t;
             te:=t+2*fws;
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             fwslope[t,k]:=0;
             for i:=ts to te do
                 fwslope[t,k]:=fwslope[t,k]+(i-t-fws)*vdtsmooth[i,k];
             fwslope[t,k]:=-3*fwslope[t,k]/(fws*(fws+1)*(2*fws+1));
             end;
     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button7Click(Sender: TObject);
var k,t,i,j,bws,ts,te:integer;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     bws:=strtoint(Edit7.Text);
     bws:=trunc(bws/2);
     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=t-2*bws;
             te:=t;
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             bwslope[t,k]:=0;
             for i:=ts to te do
                 bwslope[t,k]:=bwslope[t,k]+(i-t+bws)*vdtsmooth[i,k];
             bwslope[t,k]:=-3*bwslope[t,k]/(bws*(bws+1)*(2*bws+1));
             end;
     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button14Click(Sender: TObject);
var k,t,i,j,slr,tx,tn,ts,te:integer;xsl,flp:double;ok:boolean;

begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     slr:=strtoint(Edit10.Text);
     flp:=strtofloat(Edit11.Text)/100;
     slr:=trunc(slr/2);

     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             ts:=t-slr;
             te:=t+slr;
             if ts<1 then ts:=1;
             if te>tmax then te:=tmax;
             ok:=true;
             i:=ts;
             while ok and (i<=te) do
                   begin
                   if lcslope[t,k]>lcslope[i,k] then ok:=false;
                   if (lcslope[t,k]=lcslope[i,k]) and (t>i) then ok:=false;
                   i:=i+1;
                   end;
             if ok then
                minslope[t,k]:=true
                else
                minslope[t,k]:=false;
             ok:=true;
             i:=ts;
             while ok and (i<=te) do
                   begin
                   if lcslope[t,k]<lcslope[i,k] then ok:=false;
                   if (lcslope[t,k]=lcslope[i,k]) and (t<i) then ok:=false;
                   i:=i+1;
                   end;
             if ok then
                maxslope[t,k]:=true
                else
                maxslope[t,k]:=false;
             end;

     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             i:=t;
             while (i>=1) and not(maxslope[i,k]) do
                   i:=i-1;
             j:=t;
             while (j<=tmax) and not(minslope[j,k]) do
                   j:=j+1;
             if i>0 then
                begin
                xsl:=abs(lcslope[i,k]);
                ts:=i;
                ok:=false;
                if abs(fwslope[t,k])<flp*xsl then
                   begin
                   ok:=true;
                   i:=ts;
                   while (i<t) and ok do
                      begin
                      if abs(fwslope[i,k])<flp*xsl then ok:=false;
                      i:=i+1;
                      end;
                   end;
                if ok then
                   topbg[t,k]:=true
                   else
                   topbg[t,k]:=false;
                end;

             if j<=tmax then
                begin
                xsl:=abs(lcslope[j,k]);
                te:=j;
                ok:=false;
                if abs(bwslope[t,k])<flp*xsl then
                   begin
                   ok:=true;
                   i:=te;
                   while (i>t) and ok do
                      begin
                      if abs(bwslope[i,k])<flp*xsl then ok:=false;
                      i:=i-1;
                      end;
                   end;
                if ok then
                   topend[t,k]:=true
                   else
                   topend[t,k]:=false;
                end;
             end;

     for k:=1 to dscount do
         for t:=1 to tmax do
             begin
             i:=t;
             while (i>=1) and not(minslope[i,k]) do
                   i:=i-1;
             j:=t;
             while (j<=tmax) and not(maxslope[j,k]) do
                   j:=j+1;
             if i>0 then
                begin
                xsl:=abs(lcslope[i,k]);
                ts:=i;
                ok:=false;
                if abs(fwslope[t,k])<flp*xsl then
                   begin
                   ok:=true;
                   i:=ts;
                   while (i<t) and ok do
                      begin
                      if abs(fwslope[i,k])<flp*xsl then ok:=false;
                      i:=i+1;
                      end;
                   end;
                if ok then
                   botbg[t,k]:=true
                   else
                   botbg[t,k]:=false;
                end;

             if j<=tmax then
                begin
                xsl:=abs(lcslope[j,k]);
                te:=j;
                ok:=false;
                if abs(bwslope[t,k])<flp*xsl then
                   begin
                   ok:=true;
                   i:=te;
                   while (i>t) and ok do
                      begin
                      if abs(bwslope[i,k])<flp*xsl then ok:=false;
                      i:=i-1;
                      end;
                   end;
                if ok then
                   botend[t,k]:=true
                   else
                   botend[t,k]:=false;
                end;
             end;
     isCalcPoints:=true;
     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];

     Label14.Caption:='Ready    ';
     Refresh;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
     Label14.Caption:='Busy    ';
     Label14.Refresh;

     tstart:=strtoint(Edit1.Text);
     tend:=strtoint(Edit2.Text);

     DrawData;
     Image1.Picture.Bitmap:=NDbmp[1];
     Image2.Picture.Bitmap:=NDbmp[2];
     Image3.Picture.Bitmap:=NDbmp[3];

     Label14.Caption:='Ready    ';

     Refresh;

end;

procedure TForm1.Button8Click(Sender: TObject);
var i,j,k,l,t,t2,w,w2:integer;ok:boolean;

begin
     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(maxslope[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(maxslope[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(maxslope[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdmxsl[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(maxslope[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,1]:=i-1;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(minslope[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(minslope[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(minslope[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdmnsl[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(minslope[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,2]:=i-1;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(topbg[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(topbg[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(topbg[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdtpbg[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(topbg[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,3]:=i-1;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(topend[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(topend[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(topend[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdtpen[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(topend[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,4]:=i-1;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(botbg[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(botbg[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(botbg[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdbtbg[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(botbg[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,5]:=i-1;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             t:=1;
             w:=1;
             i:=1;
             while (t<=tmax) and (not(botend[t,k])) do
                   t:=t+1;
             while (w<=tmax) and (not(botend[w,l])) do
                   w:=w+1;
             while (t<=tmax) do
                   begin
                   ok:=false;
                   while not(ok) do
                         begin
                         w2:=w+1;
                         while (w2<=tmax) and (not(botend[w2,l])) do
                               w2:=w2+1;
                         if abs(t-w2)<abs(t-w) then
                            w:=w2
                            else
                            ok:=true;
                         end;
                   if w<=tmax then
                      begin
                      tdbten[k,l,i]:=t-w;
                      i:=i+1;
                      end;
                   t:=t+1;
                   while (t<=tmax) and (not(botend[t,k])) do
                         t:=t+1;
                   end;
             imax[k,l,6]:=i-1;
             end;

end;

procedure TForm1.Button12Click(Sender: TObject);
var i,j,k,l,mn,mx:integer;

begin
     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,1]:=0;
             stdv[k,l,1]:=0;
             for i:=1 to imax[k,l,1] do
                 begin
                 if tdmxsl[k,l,i]<mn then
                    mn:=tdmxsl[k,l,i];
                 if tdmxsl[k,l,i]>mx then
                    mx:=tdmxsl[k,l,i];
                 mean[k,l,1]:=mean[k,l,1]+tdmxsl[k,l,i];
                 stdv[k,l,1]:=stdv[k,l,1]+sqr(tdmxsl[k,l,i]);
                 end;
             mean[k,l,1]:=mean[k,l,1]/imax[k,l,1];
             stdv[k,l,1]:=stdv[k,l,1]/imax[k,l,1];
             stdv[k,l,1]:=stdv[k,l,1]-sqr(mean[k,l,1]);
             stdv[k,l,1]:=sqrt(abs(stdv[k,l,1]));
             for j:=1 to 10 do
                 drmxsl[k,l,j]:=0;
             for i:=1 to imax[k,l,1] do
                 begin
                 j:=trunc(10*(tdmxsl[k,l,i]-mn)/(mx-mn))+1;
                 drmxsl[k,l,j]:=drmxsl[k,l,j]+1;
                 end;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,2]:=0;
             stdv[k,l,2]:=0;
             for i:=1 to imax[k,l,2] do
                 begin
                 if tdmnsl[k,l,i]<mn then
                    mn:=tdmnsl[k,l,i];
                 if tdmnsl[k,l,i]>mx then
                    mx:=tdmnsl[k,l,i];
                 mean[k,l,2]:=mean[k,l,2]+tdmnsl[k,l,i];
                 stdv[k,l,2]:=stdv[k,l,2]+sqr(tdmnsl[k,l,i]);
                 end;
             mean[k,l,2]:=mean[k,l,2]/imax[k,l,2];
             stdv[k,l,2]:=stdv[k,l,2]/imax[k,l,2];
             stdv[k,l,2]:=stdv[k,l,2]-sqr(mean[k,l,2]);
             stdv[k,l,2]:=sqrt(abs(stdv[k,l,2]));
             for j:=1 to 10 do
                 drmnsl[k,l,j]:=0;
             for i:=1 to imax[k,l,2] do
                 begin
                 j:=trunc(10*(tdmnsl[k,l,i]-mn)/(mx-mn))+1;
                 drmnsl[k,l,j]:=drmnsl[k,l,j]+1;
                 end;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,3]:=0;
             stdv[k,l,3]:=0;
             for i:=1 to imax[k,l,3] do
                 begin
                 if tdtpbg[k,l,i]<mn then
                    mn:=tdtpbg[k,l,i];
                 if tdtpbg[k,l,i]>mx then
                    mx:=tdtpbg[k,l,i];
                 mean[k,l,3]:=mean[k,l,3]+tdtpbg[k,l,i];
                 stdv[k,l,3]:=stdv[k,l,3]+sqr(tdtpbg[k,l,i]);
                 end;
             mean[k,l,3]:=mean[k,l,3]/imax[k,l,3];
             stdv[k,l,3]:=stdv[k,l,3]/imax[k,l,3];
             stdv[k,l,3]:=stdv[k,l,3]-sqr(mean[k,l,3]);
             stdv[k,l,3]:=sqrt(abs(stdv[k,l,3]));
             for j:=1 to 10 do
                 drtpbg[k,l,j]:=0;
             for i:=1 to imax[k,l,3] do
                 begin
                 j:=trunc(10*(tdtpbg[k,l,i]-mn)/(mx-mn))+1;
                 drtpbg[k,l,j]:=drtpbg[k,l,j]+1;
                 end;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,4]:=0;
             stdv[k,l,4]:=0;
             for i:=1 to imax[k,l,4] do
                 begin
                 if tdtpen[k,l,i]<mn then
                    mn:=tdtpen[k,l,i];
                 if tdtpen[k,l,i]>mx then
                    mx:=tdtpen[k,l,i];
                 mean[k,l,4]:=mean[k,l,4]+tdtpen[k,l,i];
                 stdv[k,l,4]:=stdv[k,l,4]+sqr(tdtpen[k,l,i]);
                 end;
             mean[k,l,4]:=mean[k,l,4]/imax[k,l,4];
             stdv[k,l,4]:=stdv[k,l,4]/imax[k,l,4];
             stdv[k,l,4]:=stdv[k,l,4]-sqr(mean[k,l,4]);
             stdv[k,l,4]:=sqrt(abs(stdv[k,l,4]));
             for j:=1 to 10 do
                 drtpen[k,l,j]:=0;
             for i:=1 to imax[k,l,4] do
                 begin
                 j:=trunc(10*(tdtpen[k,l,i]-mn)/(mx-mn))+1;
                 drtpen[k,l,j]:=drtpen[k,l,j]+1;
                 end;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,5]:=0;
             stdv[k,l,5]:=0;
             for i:=1 to imax[k,l,5] do
                 begin
                 if tdbtbg[k,l,i]<mn then
                    mn:=tdbtbg[k,l,i];
                 if tdbtbg[k,l,i]>mx then
                    mx:=tdbtbg[k,l,i];
                 mean[k,l,5]:=mean[k,l,5]+tdbtbg[k,l,i];
                 stdv[k,l,5]:=stdv[k,l,5]+sqr(tdbtbg[k,l,i]);
                 end;
             mean[k,l,5]:=mean[k,l,5]/imax[k,l,5];
             stdv[k,l,5]:=stdv[k,l,5]/imax[k,l,5];
             stdv[k,l,5]:=stdv[k,l,5]-sqr(mean[k,l,5]);
             stdv[k,l,5]:=sqrt(abs(stdv[k,l,5]));
             for j:=1 to 10 do
                 drbtbg[k,l,j]:=0;
             for i:=1 to imax[k,l,5] do
                 begin
                 j:=trunc(10*(tdbtbg[k,l,i]-mn)/(mx-mn))+1;
                 drbtbg[k,l,j]:=drbtbg[k,l,j]+1;
                 end;
             end;

     for k:=1 to dscount do
         for l:=1 to dscount do
          if k<>l then
             begin
             mn:=10000;
             mx:=-10000;
             mean[k,l,6]:=0;
             stdv[k,l,6]:=0;
             for i:=1 to imax[k,l,6] do
                 begin
                 if tdbten[k,l,i]<mn then
                    mn:=tdbten[k,l,i];
                 if tdbten[k,l,i]>mx then
                    mx:=tdbten[k,l,i];
                 mean[k,l,6]:=mean[k,l,6]+tdbten[k,l,i];
                 stdv[k,l,6]:=stdv[k,l,6]+sqr(tdbten[k,l,i]);
                 end;
             mean[k,l,6]:=mean[k,l,6]/imax[k,l,6];
             stdv[k,l,6]:=stdv[k,l,6]/imax[k,l,6];
             stdv[k,l,6]:=stdv[k,l,6]-sqr(mean[k,l,6]);
             stdv[k,l,6]:=sqrt(abs(stdv[k,l,6]));
             for j:=1 to 10 do
                 drbten[k,l,j]:=0;
             for i:=1 to imax[k,l,6] do
                 begin
                 j:=trunc(10*(tdbten[k,l,i]-mn)/(mx-mn))+1;
                 drbten[k,l,j]:=drbten[k,l,j]+1;
                 end;
             end;
end;

procedure TForm1.Button10Click(Sender: TObject);
var f:TextFile;imx,k,l,i,j,mxv:integer;

begin
     mxv:=StrToInt(Edit9.Text);
     AssignFile(f,'distributions.txt');
     Rewrite(f);
     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,1]>imx then
                imx:=imax[k,l,1];
     writeln(f,'Time Diff Max Slope');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,1]>=i then
                       write(f,tdmxsl[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,2]>imx then
                imx:=imax[k,l,2];
     writeln(f,'Time Diff Min Slope');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,2]>=i then
                       write(f,tdmnsl[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,3]>imx then
                imx:=imax[k,l,3];
     writeln(f,'Time Diff Top Begin');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,3]>=i then
                       write(f,tdtpbg[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,4]>imx then
                imx:=imax[k,l,4];
     writeln(f,'Time Diff Top End');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,4]>=i then
                       write(f,tdtpen[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,5]>imx then
                imx:=imax[k,l,5];
     writeln(f,'Time Diff Bottom Begin');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,5]>=i then
                       write(f,tdbtbg[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,6]>imx then
                imx:=imax[k,l,6];
     writeln(f,'Time Diff Bottom End');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if imax[k,l,6]>=i then
                       write(f,tdbten[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

{Filtered by max value}

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,1]>imx then
                imx:=imax[k,l,1];
     writeln(f,'Time Diff Max Slope');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,1]>=i) and (abs(tdmxsl[k,l,i])<=mxv) then
                       write(f,tdmxsl[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,2]>imx then
                imx:=imax[k,l,2];
     writeln(f,'Time Diff Min Slope');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,2]>=i) and (abs(tdmnsl[k,l,i])<=mxv) then
                       write(f,tdmnsl[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,3]>imx then
                imx:=imax[k,l,3];
     writeln(f,'Time Diff Top Begin');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,3]>=i) and (abs(tdtpbg[k,l,i])<=mxv) then
                       write(f,tdtpbg[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,4]>imx then
                imx:=imax[k,l,4];
     writeln(f,'Time Diff Top End');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,4]>=i) and (abs(tdtpen[k,l,i])<=mxv) then
                       write(f,tdtpen[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,5]>imx then
                imx:=imax[k,l,5];
     writeln(f,'Time Diff Bottom Begin');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,5]>=i) and (abs(tdbtbg[k,l,i])<=mxv) then
                       write(f,tdbtbg[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     imx:=0;
     for k:=1 to dscount do
         for l:=1 to dscount do
             if imax[k,l,6]>imx then
                imx:=imax[k,l,6];
     writeln(f,'Time Diff Bottom End');
     for k:=1 to dscount do
         for l:=1 to dscount do
             if k<>l then
                write(f,'N',k,'-N',l,chr(9));
     writeln(f);
     for i:=1 to imx do
         begin
         for k:=1 to dscount do
             for l:=1 to dscount do
                 if k<>l then
                    if (imax[k,l,6]>=i) and (abs(tdbten[k,l,i])<=mxv) then
                       write(f,tdbten[k,l,i],chr(9))
                       else
                       write(f,' ',chr(9));
         writeln(f);
         end;
     writeln(f);
     writeln(f);
     writeln(f);

     CloseFile(f);
end;


begin
NDbmp[1]:=TBitmap.Create;
NDbmp[2]:=TBitmap.Create;
NDbmp[3]:=TBitmap.Create;
NDbmp[4]:=TBitmap.Create;
NDbmp[1].Height:=200;
NDbmp[2].Height:=200;
NDbmp[3].Height:=200;
NDbmp[4].Height:=200;
NDbmp[1].Width:=400;
NDbmp[2].Width:=400;
NDbmp[3].Width:=400;
NDbmp[4].Width:=400;



end.
